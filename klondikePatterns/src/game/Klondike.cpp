#include "Klondike.hpp"

Klondike::Klondike() {
    board = new Board();
    boardView = new BoardView(board);
    menu = new Menu(board, boardView);
}

Klondike::~Klondike() {
    delete menu;
    menu = nullptr;
    delete board;
    board = nullptr;
}

void Klondike::play(void) {
    menu->execute();
}
