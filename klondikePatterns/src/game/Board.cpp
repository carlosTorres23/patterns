#include "Board.hpp"

Board::Board() {
    init();
}

void Board::init() {
    deck.init();
    waste.init();
    //foundations.init();
    tableau.init(&deck);
}

Deck* Board::getDeck() {
    return &deck;
}

Waste* Board::getWaste() {
    return &waste;
}

Foundations* Board::getFoundations() {
    return &foundations;
}

Tableau* Board::getTableau() {
    return &tableau;
}
