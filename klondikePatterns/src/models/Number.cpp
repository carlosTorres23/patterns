#include <cassert>
#include "Number.hpp"

std::map<std::string, Number*> Number::strToNumber;

Number Number::ACE("A",0);
Number Number::TWO("2",1);
Number Number::THREE("3",2);
Number Number::FOUR("4",3);
Number Number::FIVE("5",4);
Number Number::SIX("6",5);
Number Number::SEVEN("7",6);
Number Number::EIGHT("8",7);
Number Number::NINE("9",8);
Number Number::TEN("X",9);
Number Number::JACK("J",10);
Number Number::QUEEN("Q",11);
Number Number::KING("K",12);

Number::Number(const std::string label, int id) {
    this->label = label;
    this->id = id;
    this->strToNumber[label] = this;
}

const std::string Number::getLabel() {
    return label;
}

Number Number::values[] = {ACE, TWO, THREE, FOUR, FIVE, SIX, SEVEN,
        EIGHT, NINE, TEN, JACK, QUEEN, KING};

int Number::getNumberOfNumbers() {
    return sizeof(values)/sizeof(values[0]);
}

int Number::getId(){
    return id;
}

bool Number::isValidNumber(std::string value) {
    return strToNumber.find(value) != strToNumber.end();
}

Number* Number::labelToNumber(std::string label) {
    assert (isValidNumber(label));
    return strToNumber[label];
}
