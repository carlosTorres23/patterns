#include <cassert>
#include <Suit.hpp>

std::map<std::string, Suit*> Suit::strToSuit;

Suit Suit::DIAMONDS("D",0);
Suit Suit::CLUBS("C",1);
Suit Suit::HEARTS("H",2);
Suit Suit::SPADES("S",3);


Suit::Suit(const std::string label, int id) {
    this->label = label;
    this->id = id;
    this->strToSuit[label] = this;
}

const std::string Suit::getLabel() {
    return label;
}

Suit Suit::values[] = {DIAMONDS, CLUBS, HEARTS, SPADES};

int Suit::getNumberOfSuits() {
    return sizeof(values)/sizeof(values[0]);
}

int Suit::getId(){
    return id;
}

bool Suit::isValidSuit(std::string value) {
    return strToSuit.find(value) != strToSuit.end();
}

Suit* Suit::labelToSuit(std::string label) {
    assert (isValidSuit(label));
    return strToSuit[label];
}
