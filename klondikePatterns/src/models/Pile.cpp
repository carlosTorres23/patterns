#include "Pile.hpp"

void Pile::init(void) {
    cards.clear();
    visible.clear();
}
bool Pile::isEmpty(void) {
    return cards.empty();
}

void Pile::push(Card card) {
    cards.push_back(card);
}

void Pile::push(Card card, bool isVisible) {
    push(card);
    this->visible[card] = isVisible;
}

Card Pile::pop(void) {
    Card topCard = cards.back();
    cards.pop_back();
    return topCard;
}

int Pile::getLength(void) {
    return cards.size();
}

bool Pile::isVisible(int position) {
    return visible[cards.at(position)];
}

bool Pile::isVisible(Card card) {
    return visible[card];
}

Card Pile::getAt(int position) {
    return cards.at(position);
}

Card Pile::top(void) {
    return cards.back();
}

void Pile::upturnTop(void) {
    if (!isEmpty()) {
        this->visible[cards.at(getLength()-1)] = true;
    }
}
