#include <cassert>
#include "Card.hpp"
#include "Suit.hpp"

Card::Card(Number* number, Suit* suit) {
    this->number = number;
    this->suit = suit;
}

Card::Card(std::string cardString) {
    assert(!cardString.empty());

    this->number = Number::labelToNumber(cardString.substr(0,1));
    this->suit = Suit::labelToSuit(cardString.substr(1,2));
}

bool Card::isValid(std::string cardString) {
    assert(!cardString.empty());

    return Number::isValidNumber(cardString.substr(0,1)) &&
             Suit::isValidSuit(cardString.substr(1,2));
}
