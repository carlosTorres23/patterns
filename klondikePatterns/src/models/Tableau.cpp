#include "Tableau.hpp"
#include "IO.hpp" // TODO: DELETE

void Tableau::init(Deck *deck) {
    for (int i=0; i < COLUMNS_NUMBER; i++) {
          pile[i].init();
      }
    for (int i=0; i < COLUMNS_NUMBER; i++) {
        pile[i].push(deck->takeCard(), true);
        for (int j=i+1; j<7; j++) {
            pile[j].push(deck->takeCard(), false);
        }
    }
}

int Tableau::getColumnsNumber() {
    return COLUMNS_NUMBER;
}
int Tableau::getRowsNumber() {
    return 7;
    //TODO: RETURN CURRENT HEIGHT.
}

bool Tableau::isEmpty(int row, int column) {
    bool isEmpty = true;
    if (row < pile[column].getLength()) {
        isEmpty = false;
    }
    return isEmpty;
}

bool Tableau::isVisible(int row, int column) {
    return pile[column].isVisible(row);
}

Card Tableau::getCard(int row, int column) {
    return pile[column].getAt(row);
}
