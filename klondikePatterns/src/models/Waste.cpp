#include "Waste.hpp"

void Waste::init() {
    pile.init();
}

bool Waste::isEmpty() {
    return pile.isEmpty();
}

Card Waste::top() {
    return pile.top();
}

void Waste::push(Card card) {
    pile.push(card, true);
}

Card Waste::takeCard() {
    return pile.pop();
}
