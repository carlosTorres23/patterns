#include "Foundations.hpp"

#include "Number.hpp" //TODO: DELETE
#include "Suit.hpp" //TODO: DELETE


int Foundations::getNumber() {
    return FOUNDATIONS_NUMBER;
}

// TODO: RETURN VALUE ACCORDING TO DATA
bool Foundations::isEmpty(int position) {
    switch (position) {
        case 1:
            return false;
        case 2:
            return false;
        case 3:
            return false;
        case 4:
            return false;
        default:
            return false;
    }
}

Card Foundations::top(int position) {
//TODO:RETURN REAL VALUES
    switch (position) {
        case 1:
            return Card(&Number::ACE, &Suit::HEARTS);
        case 2:
            return Card(&Number::TWO, &Suit::HEARTS);
        case 3:
            return Card(&Number::THREE, &Suit::HEARTS);
        case 4:
            return Card(&Number::FOUR, &Suit::HEARTS);
        default:
            return Card(&Number::KING, &Suit::HEARTS);
    }
}
