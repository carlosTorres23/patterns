#include "Deck.hpp"

#include "Number.hpp"
#include "Card.hpp"
#include "Suit.hpp"

void Deck::init() {
    cards.init();

    std::vector<Card> shuffledCards = createShuffledDeck();

    for (std::vector<Card>::iterator it=shuffledCards.begin();
            it!=shuffledCards.end();it++) {
        cards.push(*it);
    }
}

std::vector <Card> Deck::createShuffledDeck() {
    std::vector<Card> newCards;
    for (int i = 0; i< Suit::getNumberOfSuits(); i++) {
        for (int j = 0; j < Number::getNumberOfNumbers(); j++) {
            Card card(&Number::values[j], &Suit::values[i]);
            newCards.push_back(card);
        }
    }

    std::srand(time(nullptr));
    std::random_shuffle(newCards.begin(), newCards.end(), randomGenerator);
    return newCards;
}

int Deck::randomGenerator(int number) {
    return std::rand()%number;
}

bool Deck::isEmpty() {
    return cards.isEmpty();
}

Card Deck::takeCard() {
    return cards.pop();
}

void Deck::push(Card card) {
    cards.push(card);
}
