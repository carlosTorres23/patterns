#include "WasteView.hpp"
#include "IO.hpp"

WasteView::WasteView() {
    cardView = CardView::instance();
}
void WasteView::write(Waste waste) {
    if (waste.isEmpty()) {
        cardView->writeEmpty();
    } else {
        cardView->write(waste.top());
    }
    IO::instance()->write("       ");
}
