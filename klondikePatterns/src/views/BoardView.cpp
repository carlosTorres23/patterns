#include "BoardView.hpp"

BoardView::BoardView(Board *board) {
   this->board = board;
}

void BoardView::write(void) {
    deckView.write(*board->getDeck());
    wasteView.write(*board->getWaste());
    foundationsView.write(*board->getFoundations());
    tableauView.write(*board->getTableau());
}
