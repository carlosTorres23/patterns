#include "IO.hpp"
#include <iostream>

IO *IO::io = nullptr;

IO* IO::instance(void) {
    if (io == nullptr) {
        io = new IO();
    }
    return io;
}

void IO::write(std::string text) {
    std::cout << text;
}

void IO::writeln(std::string text) {
    std::cout << text << std::endl;
}

int IO::readInt(std::string message) {
    int value;
    write(message);
    std::cin >> value;
    std::cin.clear();
    std::cin.ignore();
    return value;
}

std::string IO::readString(std::string message) {
    std::string value;
    write(message);
    std::cin >> value;
    std::cin.clear();
    std::cin.ignore();
    return value;

}
