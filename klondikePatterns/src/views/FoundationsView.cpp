#include "FoundationsView.hpp"
#include "IO.hpp"

FoundationsView::FoundationsView() {
    cardView = CardView::instance();
}

void FoundationsView::write(Foundations foundations) {
    int foundationsNumber = foundations.getNumber();
    for (int i = 1; i <= foundationsNumber; i++) {
        if (foundations.isEmpty(i)) {
            cardView->writeEmpty();
        } else {
            cardView->write(foundations.top(i));
        }
    }
    IO::instance()->writeln("");
    IO::instance()->writeln("");
}
