#include "TableauView.hpp"
#include "IO.hpp"

TableauView::TableauView() {
    cardView = CardView::instance();
}

void TableauView::write(Tableau tableau) {
    int rowsNumber = tableau.getRowsNumber();
    int columnsNumber = tableau.getColumnsNumber();

    for (int i = 0; i < rowsNumber; i++) {
        for (int j = 0; j < columnsNumber; j++) {
            if (tableau.isEmpty(i, j)) {
                cardView->writeEmpty();
            } else if (!tableau.isVisible(i,j)){
                cardView->writeHidden();
            } else {
                cardView->write(tableau.getCard(i, j));
            }
        }
        IO::instance()->writeln("");
    }
}
