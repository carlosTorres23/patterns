#include "DeckView.hpp"

DeckView::DeckView() {
    cardView = CardView::instance();
}
void DeckView::write(Deck deck) {
    if (deck.isEmpty()) {
        cardView->writeEmpty();
    } else {
        cardView->writeHidden();
    }
}
