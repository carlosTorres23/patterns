#include <CardView.hpp>
#include "IO.hpp"

CardView *CardView::cardView = nullptr;

CardView::CardView() {
    io = IO::instance();
}

CardView* CardView::instance(void) {
    if (cardView == nullptr) {
        cardView = new CardView();
    }
    return cardView;
}

void CardView::writeEmpty() {
    io->write("       ");
}

void CardView::writeHidden() {
    io->write("[XXXX] ");
}

void CardView::write(Card card) {
    io->write("[ " +
            card.getNumber()->getLabel() +
            card.getSuit()->getLabel() + " ] ");
}
