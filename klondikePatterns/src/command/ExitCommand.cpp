#include "ExitCommand.hpp"
#include "IO.hpp" // TODO: DELETE

ExitCommand::ExitCommand() : Command("Exit.", nullptr, nullptr) {
    exitExecuted = false;
}

bool ExitCommand::isActive() {
    return true;
}

void ExitCommand::execute() {
    exitExecuted = true;
}

bool ExitCommand::isExecuted() {
    return exitExecuted;
}
