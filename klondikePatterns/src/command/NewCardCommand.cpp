#include "NewCardCommand.hpp"

NewCardCommand::NewCardCommand(Board *board, CommandHistory *commandHistory) :
    UndoableCommand("New Card.", board, commandHistory) {
}

bool NewCardCommand::isActive() {
    return !(board->getDeck()->isEmpty());
}

void NewCardCommand::execute() {
    board->getWaste()->push(board->getDeck()->takeCard());
    commandHistory->save(this->clone());
}

void NewCardCommand::undo() {
    board->getDeck()->push(board->getWaste()->takeCard());
}

void NewCardCommand::redo() {
    board->getWaste()->push(board->getDeck()->takeCard());
}

UndoableCommand* NewCardCommand::clone() {
    NewCardCommand *cloneCommand = new NewCardCommand(board, commandHistory);
    return cloneCommand;
}
