#include "RedoCommand.hpp"

RedoCommand::RedoCommand(Board *board, CommandHistory *commandHistory) :
    Command("Redo.", board, commandHistory){
}

bool RedoCommand::isActive() {
    return commandHistory->isRedoAvailable();
}

void RedoCommand::execute() {
    commandHistory->redo();
}
