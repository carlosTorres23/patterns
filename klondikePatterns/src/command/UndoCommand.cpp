#include "UndoCommand.hpp"

UndoCommand::UndoCommand(Board *board, CommandHistory *commandHistory) :
    Command ("Undo.", board, commandHistory) {
}

bool UndoCommand::isActive() {
    return commandHistory->isUndoAvailable();
}

void UndoCommand::execute() {
    commandHistory->undo();
}
