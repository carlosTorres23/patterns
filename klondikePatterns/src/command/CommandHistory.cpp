#include "CommandHistory.hpp"

CommandHistory::CommandHistory() {
    firstPrevious = 0;
}

void CommandHistory::save(UndoableCommand *undoableCommand) {
    for (int i = 0; i < firstPrevious; i++) {
        commandList.erase(commandList.begin());
    }
    firstPrevious = 0;
    commandList.push_front(undoableCommand);
}

void CommandHistory::undo() {
    commandList[firstPrevious]->undo();
    firstPrevious++;
}

void CommandHistory::redo() {
    firstPrevious--;
    commandList[firstPrevious]->redo();
}

bool CommandHistory::isUndoAvailable() {
    return firstPrevious < (int)commandList.size();

}

bool CommandHistory::isRedoAvailable() {
    return firstPrevious > 0;
}
