#include "MoveCommand.hpp"
#include "IO.hpp" // TODO: DELETE
#include "CommandDialog.hpp"


MoveCommand::MoveCommand(Board *board, CommandHistory *commandHistory) :
    UndoableCommand("Move card.", board, commandHistory) {
}

bool MoveCommand::isActive() {
    // TODO: return true if movement is possible.
    return true;
}

void MoveCommand::execute() {
    IO::instance()->writeln("MoveCommand execute."); // TODO: DELETE
    Card originCard = CommandDialog::instance()->readOrigin();
    std::string destination = CommandDialog::instance()->readDestination();

    //TODO: CONTINUE MOVEMENT
    IO::instance()->writeln("CARD Selected: " +
            originCard.getNumber()->getLabel() +
            originCard.getSuit()->getLabel() +
            " DESTINATION: " +
            destination);

}

void MoveCommand::undo() {

}

void MoveCommand::redo() {

}

UndoableCommand* MoveCommand::clone() {
    return nullptr;
}
