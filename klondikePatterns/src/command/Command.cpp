#include "Command.hpp"

Command::Command(std::string title,
        Board *board, CommandHistory *commandHistory) {
    this->title = title;
    this->board = board;
    this->commandHistory = commandHistory;
}

std::string Command::getTitle() {
    return title;
}
