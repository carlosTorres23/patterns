#include "NewGameCommand.hpp"

NewGameCommand::NewGameCommand(Board *board) :
    Command("New game.", board, nullptr) {
}

bool NewGameCommand::isActive() {
    return true;
}

void NewGameCommand::execute() {
    board->init();
}
