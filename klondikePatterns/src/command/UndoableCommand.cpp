#include "UndoableCommand.hpp"

UndoableCommand::UndoableCommand(
        std::string title, Board *board, CommandHistory *commandHistory) :
        Command(title, board, commandHistory) {
}
