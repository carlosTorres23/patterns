#include "CommandDialog.hpp"
#include "Card.hpp"
#include "IO.hpp"

CommandDialog *CommandDialog::commandDialog = nullptr;

CommandDialog* CommandDialog::instance(void) {
    if (commandDialog == nullptr) {
        commandDialog = new CommandDialog();
    }
    return commandDialog;
}

int CommandDialog::read(int min, int max) {
    int value;
    bool ok;
    do {
        value = IO::instance()->readInt("Select option "
                "(" + std::to_string(min) +
                "-" + std::to_string(max) + "): ");
        ok = (min <= value && value <= max);
        if (!ok) {
            IO::instance()->writeln("Invalid value.");
        }
    } while (!ok);
    return value;
}

Card CommandDialog::readOrigin(){
    std::string value;
    bool ok;
    do {
            value = IO::instance()->readString("Select card to move "
                    "(NS: where N is number and S is suit): ");
            ok = Card::isValid(value);
            if (!ok) {
                IO::instance()->writeln("Invalid value.");
            }
        } while (!ok);
    Card card(value);
    return card;
}

std::string CommandDialog::readDestination(){
    std::string value;
    bool ok;
    do {
             value = IO::instance()->readString("Select destination "
                     "(Tt | Ff : where t is Tableau column and f is Foundation position): ");
             ok = isValidDestination(value);
             if (!ok) {
                 IO::instance()->writeln("Invalid value.");
             }
         } while (!ok);
    return value;
}

//TODO: THIS CHECK SHOULD BE IN MODEL ? VIEW ? I DON'T LIKE HERE.
bool CommandDialog::isValidDestination(std::string destination) {

    bool result=false;
    char stackType;
    int position;
    sscanf(destination.c_str(), "%c%d", &stackType, &position);

    if (stackType == 'T') {
        result = (position > 0 && position < 8);
    } else if (stackType == 'F') {
        result = (position > 0 && position < 5);
    }
    return result;
}
