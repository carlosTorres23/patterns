#include <cassert>
#include "Menu.hpp"
#include "NewCardCommand.hpp"
#include "MoveCommand.hpp"
#include "NewGameCommand.hpp"
#include "UndoCommand.hpp"
#include "RedoCommand.hpp"
#include "IO.hpp"
#include "CommandDialog.hpp"

Menu::Menu(Board *board, BoardView *boardView) {
    assert(board != nullptr);
    assert(boardView != nullptr);
    this->board = board;
    this->boardView = boardView;
    commandHistory = new CommandHistory();
    allCommandList.push_back(new NewCardCommand(board, commandHistory));
    allCommandList.push_back(new MoveCommand(board, commandHistory));
    allCommandList.push_back(new UndoCommand(board, commandHistory));
    allCommandList.push_back(new RedoCommand(board, commandHistory));
    allCommandList.push_back(new NewGameCommand(board));
    exitCommand = new ExitCommand();
    allCommandList.push_back(exitCommand);
}


void Menu::execute() {
    do {
        this->setCurrentCommands();
        boardView->write();
        this->write();
        int selectedCommand = getSelectedCommand();
        currentCommandList[selectedCommand]->execute();
    } while (!exitCommand->isExecuted());
}

void Menu::setCurrentCommands() {
    currentCommandList.clear();
    for (std::vector<Command*>::iterator it=allCommandList.begin();
            it != allCommandList.end(); it++) {
        if ((*it)->isActive()) {
            currentCommandList.push_back(*it);
        }
    }
}

void Menu::write() {
    IO *io = IO::instance();
    io->writeln("");
    for (int i = 0; i < (int)currentCommandList.size(); i++) {
        io->writeln(std::to_string(i+1) + ". " + currentCommandList[i]->getTitle());
    }
}

int Menu::getSelectedCommand() {
    return CommandDialog::instance()->read(1, (int)currentCommandList.size()) - 1;
}

