#ifndef INCLUDE_BOARD_HPP_
#define INCLUDE_BOARD_HPP_

#include "Deck.hpp"
#include "Waste.hpp"
#include "Foundations.hpp"
#include "Tableau.hpp"

class Board {
public:
    Board();
    void init();
    Deck* getDeck();
    Waste* getWaste();
    Foundations* getFoundations();
    Tableau* getTableau();

private:
    Deck deck;
    Waste waste;
    Foundations foundations;
    Tableau tableau;
};

#endif /* INCLUDE_BOARD_HPP_ */
