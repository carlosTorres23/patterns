#ifndef INCLUDE_KLONDIKE_HPP_
#define INCLUDE_KLONDIKE_HPP_

#include "Board.hpp"
#include "BoardView.hpp"
#include "Menu.hpp"

class Klondike {
public:
    void play();
    Klondike();
    ~Klondike();

private:
    Board *board;
    BoardView *boardView;
    Menu *menu;
};

#endif /* INCLUDE_KLONDIKE_HPP_ */
