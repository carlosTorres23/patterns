#ifndef INCLUDE_NEWCARDCOMMAND_HPP_
#define INCLUDE_NEWCARDCOMMAND_HPP_

#include "UndoableCommand.hpp"
#include "Board.hpp"
#include "CommandHistory.hpp"

class NewCardCommand : public UndoableCommand {
public:
    NewCardCommand(Board*, CommandHistory*);
    bool isActive();
    void execute();
    void undo();
    void redo();
    UndoableCommand* clone();
};

#endif /* INCLUDE_NEWCARDCOMMAND_HPP_ */
