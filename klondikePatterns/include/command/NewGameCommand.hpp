#ifndef INCLUDE_NEWGAMECOMMAND_HPP_
#define INCLUDE_NEWGAMECOMMAND_HPP_

#include "Command.hpp"
#include "Board.hpp"

class NewGameCommand : public Command {
public:
    NewGameCommand(Board*);
    bool isActive();
    void execute();

};

#endif /* INCLUDE_NEWGAMECOMMAND_HPP_ */
