#ifndef INCLUDE_COMMANDHISTORY_HPP_
#define INCLUDE_COMMANDHISTORY_HPP_

#include <deque>
#include "UndoableCommand.hpp"

class CommandHistory {

public:
    CommandHistory();
    void save(UndoableCommand*);

    void undo();
    void redo();
    bool isUndoAvailable();
    bool isRedoAvailable();

private:
    std::deque<UndoableCommand*> commandList;
    int firstPrevious;
};

#endif /* INCLUDE_COMMANDHISTORY_HPP_ */
