#ifndef INCLUDE_EXITCOMMAND_HPP_
#define INCLUDE_EXITCOMMAND_HPP_

#include "Command.hpp"

class ExitCommand : public Command {
public:
    ExitCommand();
    bool isActive();
    void execute();
    bool isExecuted();
private:
    bool exitExecuted;
};


#endif /* INCLUDE_EXITCOMMAND_HPP_ */
