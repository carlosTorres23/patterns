#ifndef INCLUDE_COMMAND_REDOCOMMAND_HPP_
#define INCLUDE_COMMAND_REDOCOMMAND_HPP_

#include "Command.hpp"
#include "Board.hpp"
#include "CommandHistory.hpp"

class RedoCommand : public Command {
public:
    RedoCommand(Board*, CommandHistory*);
    bool isActive();
    void execute();
};

#endif /* INCLUDE_COMMAND_REDOCOMMAND_HPP_ */
