#ifndef INCLUDE_COMMAND_UNDOABLECOMMAND_HPP_
#define INCLUDE_COMMAND_UNDOABLECOMMAND_HPP_

#include "Command.hpp"
#include "Board.hpp"

class CommandHistory;

class UndoableCommand : public Command {

public:
    virtual void undo() = 0;
    virtual void redo() = 0;
    virtual UndoableCommand* clone() = 0;
    virtual ~UndoableCommand() {}

protected:
    UndoableCommand(std::string title, Board*, CommandHistory*);
};

#endif /* INCLUDE_COMMAND_UNDOABLECOMMAND_HPP_ */
