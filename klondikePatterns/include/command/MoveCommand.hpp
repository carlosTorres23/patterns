#ifndef INCLUDE_MOVECOMMAND_HPP_
#define INCLUDE_MOVECOMMAND_HPP_

#include "UndoableCommand.hpp"
#include "Board.hpp"
#include "CommandHistory.hpp"

class MoveCommand : public UndoableCommand {
public:
    MoveCommand(Board*, CommandHistory*);
    bool isActive();
    void execute();
    void undo();
    void redo();
    UndoableCommand* clone();

};

#endif /* INCLUDE_MOVECOMMAND_HPP_ */
