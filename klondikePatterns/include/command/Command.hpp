#ifndef INCLUDE_COMMAND_HPP_
#define INCLUDE_COMMAND_HPP_

#include <iostream>
#include "Board.hpp"
class CommandHistory;

class Command {
public:
    Command(std::string title, Board*, CommandHistory*);
    virtual ~Command() {}
    virtual bool isActive() = 0;
    std::string getTitle();
    virtual void execute() = 0;

private:
    std::string title;

protected:
    Board *board;
    CommandHistory *commandHistory;
};

#endif /* INCLUDE_COMMAND_HPP_ */
