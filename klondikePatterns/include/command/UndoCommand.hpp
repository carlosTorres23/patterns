#ifndef INCLUDE_COMMAND_UNDOCOMMAND_HPP_
#define INCLUDE_COMMAND_UNDOCOMMAND_HPP_

#include "Command.hpp"
#include "Board.hpp"
#include "CommandHistory.hpp"

class UndoCommand : public Command {
public:
    UndoCommand(Board*, CommandHistory*);
    bool isActive();
    void execute();
};

#endif /* INCLUDE_COMMAND_UNDOCOMMAND_HPP_ */
