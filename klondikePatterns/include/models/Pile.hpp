#ifndef INCLUDE_PILE_HPP_
#define INCLUDE_PILE_HPP_

#include <deque>
#include <map>

#include "Card.hpp"

class Pile {
public:
    void init(void);
    bool isEmpty();
    void push(Card);
    void push(Card, bool visible);
    Card pop(void);
    Card top(void);
    int getLength();
    bool isVisible(int position);
    bool isVisible(Card);
    Card getAt(int position);
    void upturnTop(void);

private:
    std::deque<Card> cards;
    std::map <Card, bool> visible;
};

#endif /* INCLUDE_PILE_HPP_ */
