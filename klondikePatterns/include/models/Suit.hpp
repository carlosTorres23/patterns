#ifndef INCLUDE_SUIT_HPP_
#define INCLUDE_SUIT_HPP_

#include <map>
#include <iostream>

class Suit {
public:
    static Suit DIAMONDS;
    static Suit CLUBS;
    static Suit HEARTS;
    static Suit SPADES;

    static Suit values[];

    static int getNumberOfSuits();

    const std::string getLabel(void);
    int getId();
    static bool isValidSuit(std::string);
    static Suit* labelToSuit(std::string);

private:
    Suit(std::string, int);
    std::string label;
    int id;
    static std::map<std::string, Suit*> strToSuit;
};
#endif /* INCLUDE_SUIT_HPP_ */
