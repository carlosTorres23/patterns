#ifndef INCLUDE_NUMBER_HPP_
#define INCLUDE_NUMBER_HPP_

#include <map>
#include <iostream>

class Number {
public:
    static Number ACE;
    static Number TWO;
    static Number THREE;
    static Number FOUR;
    static Number FIVE;
    static Number SIX;
    static Number SEVEN;
    static Number EIGHT;
    static Number NINE;
    static Number TEN;
    static Number JACK;
    static Number QUEEN;
    static Number KING;

    static Number values[];

    static int getNumberOfNumbers();
    const std::string getLabel(void);
    int getId();
    static bool isValidNumber(std::string);
    static Number* labelToNumber(std::string);

private:
    Number(std::string, int);
    std::string label;
    int id;
    static std::map<std::string, Number*> strToNumber;

};
#endif /* INCLUDE_NUMBER_HPP_ */
