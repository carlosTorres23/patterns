#ifndef INCLUDE_CARD_HPP_
#define INCLUDE_CARD_HPP_

#include "Number.hpp"
#include "Suit.hpp"

class Card {

public:
    Card(Number*, Suit*);
    Card(std::string);
    static bool isValid(std::string);
    Number* stringNumberToNumber(std::string);
    Suit* stringSuitToSuit(std::string);

    Number* getNumber(void) const { return this->number; };
    Suit* getSuit(void) const { return this->suit; };

    bool operator==(const Card& other) const {
        return (number->getLabel() == other.number->getLabel() &&
                (suit->getLabel() == other.suit->getLabel()));
    }

    bool operator <(const Card& other) const {
        return (number->getId() + suit->getId()*13) <
                (other.number->getId() + other.suit->getId()*13);
    }


private:
    Number* number;
    Suit* suit;
};

#endif /* INCLUDE_CARD_HPP_ */
