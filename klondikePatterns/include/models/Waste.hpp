#ifndef INCLUDE_MODELS_WASTE_HPP_
#define INCLUDE_MODELS_WASTE_HPP_

#include "Card.hpp"
#include "Pile.hpp"

class Waste {
public:
    void init();
    bool isEmpty();
    Card top();
    void push(Card);
    Card takeCard();

private:
    Pile pile;
};

#endif /* INCLUDE_MODELS_WASTE_HPP_ */
