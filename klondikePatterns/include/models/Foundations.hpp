#ifndef INCLUDE_MODELS_FOUNDATIONS_HPP_
#define INCLUDE_MODELS_FOUNDATIONS_HPP_

#include "Card.hpp"

class Foundations {
public:
    bool isEmpty(int position);
    int getNumber();
    Card top(int position);
private:
    static const int FOUNDATIONS_NUMBER = 4;
};



#endif /* INCLUDE_MODELS_FOUNDATIONS_HPP_ */
