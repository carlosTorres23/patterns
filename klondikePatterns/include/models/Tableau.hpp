#ifndef INCLUDE_MODELS_TABLEAU_HPP_
#define INCLUDE_MODELS_TABLEAU_HPP_

#include "Deck.hpp"
#include "Card.hpp"
#include "Pile.hpp"

class Tableau {
public:
    void init(Deck* deck);
    int getRowsNumber();
    int getColumnsNumber();
    bool isEmpty(int row, int column);
    bool isVisible(int row, int column);
    Card getCard(int row, int column);

private:
    static const int COLUMNS_NUMBER = 7;
    Pile pile[COLUMNS_NUMBER];
};

#endif /* INCLUDE_MODELS_TABLEAU_HPP_ */
