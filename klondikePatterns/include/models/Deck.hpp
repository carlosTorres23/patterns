#ifndef INCLUDE_MODELS_DECK_HPP_
#define INCLUDE_MODELS_DECK_HPP_

#include <vector>
#include "Card.hpp"
#include "Pile.hpp"

class Deck {
public:
    void init();
    bool isEmpty();
    Card takeCard();
    void push(Card);
private:
    Pile cards;
    std::vector <Card> createShuffledDeck();
    static int randomGenerator(int);
};

#endif /* INCLUDE_MODELS_DECK_HPP_ */
