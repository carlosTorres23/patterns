#ifndef INCLUDE_MENU_HPP_
#define INCLUDE_MENU_HPP_

#include <vector>
#include "Board.hpp"
#include "BoardView.hpp"
#include "Command.hpp"
#include "CommandHistory.hpp"
#include "ExitCommand.hpp"

class Menu {
public:
    Menu(Board*, BoardView*);
    void execute();
private:
    Board *board;
    BoardView *boardView;
    CommandHistory *commandHistory;
    std::vector<Command*> allCommandList;
    std::vector<Command*> currentCommandList;
    ExitCommand *exitCommand;
    void setCurrentCommands();
    int getSelectedCommand();
    void write();

};

#endif /* INCLUDE_MENU_HPP_ */
