#ifndef INCLUDE_MENU_COMMANDDIALOG_HPP_
#define INCLUDE_MENU_COMMANDDIALOG_HPP_

#include <iostream>
#include "Card.hpp"

class CommandDialog {

public:
    static CommandDialog* instance();
    int read(int min, int max);
    Card readOrigin();
    std::string readDestination();

private:
    static CommandDialog *commandDialog;
    CommandDialog() {}
    bool isValidDestination(std::string);
};

#endif /* INCLUDE_MENU_COMMANDDIALOG_HPP_ */
