#ifndef INCLUDE_VIEWS_WASTEVIEW_HPP_
#define INCLUDE_VIEWS_WASTEVIEW_HPP_

#include "Waste.hpp"
#include "CardView.hpp"

class WasteView {
public:
    WasteView();
    void write(Waste);
private:
    CardView *cardView;
};



#endif /* INCLUDE_VIEWS_WASTEVIEW_HPP_ */
