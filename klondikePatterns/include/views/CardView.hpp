#ifndef INCLUDE_VIEWS_CARDVIEW_HPP_
#define INCLUDE_VIEWS_CARDVIEW_HPP_

#include <iostream>
#include "Card.hpp"
#include "io.hpp"

class CardView {
public:
    static CardView* instance();

    void writeEmpty();
    void writeHidden();
    void write (Card);

private:
    static CardView *cardView;
    IO *io;
    CardView();
};

#endif /* INCLUDE_VIEWS_CARDVIEW_HPP_ */

