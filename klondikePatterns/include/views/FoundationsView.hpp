#ifndef INCLUDE_VIEWS_FOUNDATIONSVIEW_HPP_
#define INCLUDE_VIEWS_FOUNDATIONSVIEW_HPP_

#include "Foundations.hpp"
#include "CardView.hpp"

class FoundationsView {
public:
    FoundationsView();
    void write(Foundations);

private:
    CardView *cardView;
};

#endif /* INCLUDE_VIEWS_FOUNDATIONSVIEW_HPP_ */
