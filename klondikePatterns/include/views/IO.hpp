#ifndef INCLUDE_IO_HPP_
#define INCLUDE_IO_HPP_

#include <iostream>

class IO {

public:
    static IO* instance();
    void write(std::string);
    void writeln(std::string);
    int readInt(std::string);
    std::string readString(std::string);

private:
    static IO *io;
    IO() {}
};

#endif /* INCLUDE_IO_HPP_ */
