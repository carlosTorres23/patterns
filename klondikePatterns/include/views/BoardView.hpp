#ifndef INCLUDE_VIEWS_BOARDVIEW_HPP_
#define INCLUDE_VIEWS_BOARDVIEW_HPP_

#include "Board.hpp"
#include "DeckView.hpp"
#include "WasteView.hpp"
#include "FoundationsView.hpp"
#include "TableauView.hpp"

class BoardView {
public:
    BoardView(Board*);
    void write();
private:
    Board *board;
    DeckView deckView;
    WasteView wasteView;
    FoundationsView foundationsView;
    TableauView tableauView;
};

#endif /* INCLUDE_VIEWS_BOARDVIEW_HPP_ */
