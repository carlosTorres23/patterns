#ifndef INCLUDE_VIEWS_TABLEAUVIEW_HPP_
#define INCLUDE_VIEWS_TABLEAUVIEW_HPP_

#include "Tableau.hpp"
#include "CardView.hpp"

class TableauView {
public:
    TableauView();
    void write(Tableau);
private:
    CardView *cardView;
};



#endif /* INCLUDE_VIEWS_TABLEAUVIEW_HPP_ */
