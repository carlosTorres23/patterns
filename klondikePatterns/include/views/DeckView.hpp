#ifndef INCLUDE_VIEWS_DECKVIEW_HPP_
#define INCLUDE_VIEWS_DECKVIEW_HPP_

#include "Deck.hpp"
#include "CardView.hpp"

class DeckView {
public:
    DeckView();
    void write(Deck);
private:
    CardView *cardView;
};

#endif /* INCLUDE_VIEWS_DECKVIEW_HPP_ */
