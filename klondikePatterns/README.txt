CONSTRUCCION DEL BINARIO
    Se genera con el Makefile, llamando a "make" sin argumentos

BINARIO DEL KLONDIKE
    Se genera en el path ./build/apps/Klondike

ESTADO FUNCIONAL 
    Falta implementar el movimiento de cartas. Estoy adaptando la lógica
    que tengo en la primera versión sin patrones
    Falta guardar y recuperar partida.

MEJORAS PENDIENTES
    - Mejorar las clases del modelo que contienen pilas para no repetir 
      código creando clase/s base que implemente/n lo común.
    - Separar la vista del modelo en las clases Number y Suit.

PATRONES PENDIENTES
    - Patrón memento para guardar/recuperar partida.
    - Patrón composite para tratar una carta o varias del mismo modo.
    - Patrón abstract factory para la creación de distintos tipos de baraja.
    - Patrón iterator para recorrer las cartas del tablero.
